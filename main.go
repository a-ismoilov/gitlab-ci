package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		_, _ = fmt.Fprintf(c.Writer, "Goodbye gitlab-ci")
	})
	r.GET("/ping", func(c *gin.Context) {
		_, _ = fmt.Fprintf(c.Writer, "pong")
	})

	_ = r.Run(":8007")
}

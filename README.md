# gitlab-ci

```description
This is small 'hello gitlab-ci', golang/gin application to test and learn gitlab ci/cd
```

```bash
make run #to run the project on port [:8007]
```

All source ci scripts are stored at `.gitlab-ci.yml` file

```bash
make build #to build binary file at ${project}/bin/...
```